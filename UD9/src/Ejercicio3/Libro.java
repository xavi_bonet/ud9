package Ejercicio3;

public class Libro {

	// ATRIBUTOS
	private String isbn;
	private String titulo;
	private String autor;
	private int numPaginas;
	
	
	// CONSTRUCTORES
	public Libro() {
		this.isbn = "";
		this.titulo = "";
		this.autor = "";
		this.numPaginas = 0;
	}
	
	public Libro(String isbn, String titulo, String autor, int numPaginas) {
		this.isbn = isbn;
		this.titulo = titulo;
		this.autor = autor;
		this.numPaginas = numPaginas;
	}

	
	//GETTERS Y SETTERS
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public int getNumPaginas() {
		return numPaginas;
	}
	public void setNumPaginas(int numPaginas) {
		this.numPaginas = numPaginas;
	}

	
	// METODOS
	@Override
	public String toString() {
		return "El libro "+ titulo +" con ISBN "+ isbn +" creado por el autor "+ autor +" tiene "+ numPaginas +" p�ginas.";
	}

	// Comparar el numero de paginas de 2 objetos de tipo libro i mostrar el objeto con mas paginas
	public void compareTo (Libro l) {
		int value1 = getNumPaginas();
		int value2 = l.getNumPaginas();	
		if (value1 > value2) {
			System.out.println("El libro con mas paginas es: ");
			System.out.println(toString());
		} else {
			System.out.println("El libro con mas paginas es: ");
			System.out.println(l.toString());
		}
	}
	
}
