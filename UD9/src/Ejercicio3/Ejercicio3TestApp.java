package Ejercicio3;

public class Ejercicio3TestApp {

	public static void main(String[] args) {
		
		// crear objetos libro
		Libro l1 = new Libro("S14DT6","Libro1","Autor1",1563);
		Libro l2 = new Libro("A13D58","Libro2","Autor2",552);
		
		// mostrar los objetos
		System.out.println(l1.toString());
		System.out.println(l2.toString());
		
		// comparar numero de paginas
		l1.compareTo(l2);

	}

}
