package Ejercicio5;

public class Ejercicio5TestApp {

	public static void main(String[] args) {
		
		Profesor Professor1 = new Profesor("matematicas", "Profesor1", 45, "Hombre");
		
		Estudiante listaEstudiantes[] = new Estudiante[10];
		listaEstudiantes[0] = new Estudiante(8, "Alumno1", 21, "Hombre");
		listaEstudiantes[1] = new Estudiante(6, "Alumno2", 21, "Hombre");
		listaEstudiantes[2] = new Estudiante(6, "Alumno3", 21, "Hombre");
		listaEstudiantes[3] = new Estudiante(5, "Alumno4", 21, "Hombre");
		listaEstudiantes[4] = new Estudiante(3, "Alumno5", 21, "Hombre");
		listaEstudiantes[5] = new Estudiante(6, "Alumna1", 21, "Mujer");
		listaEstudiantes[6] = new Estudiante(9, "Alumna2", 21, "Mujer");
		listaEstudiantes[7] = new Estudiante(4, "Alumna3", 21, "Mujer");
		listaEstudiantes[8] = new Estudiante(1, "Alumna4", 21, "Mujer");
		listaEstudiantes[9] = new Estudiante(8, "Alumna5", 21, "Mujer");
		
		Aula aula1 = new Aula(1, 10, "matematicas", Professor1, listaEstudiantes);
		
		int totalAlumnosClase = 0;
		for (int i = 0; listaEstudiantes.length > i; i++) {
			if (!listaEstudiantes[i].getFaltaAssistencia()) {
				totalAlumnosClase++;
			}
		}
		
		System.out.println("Hay clase hoy ?");
		aula1.comprovarSiHayClase(Professor1.getFaltaAssistencia(), Professor1.getMateriaEnseñada(), 10, totalAlumnosClase);
		
		int alumnosAprovados = 0;
		int alumnasAprovadas = 0;
		if (aula1.getCalaseSiNo()) {
			for (int i = 0; listaEstudiantes.length > i; i++) {
				if (listaEstudiantes[i].getSexo() == "Hombre") {
					if (listaEstudiantes[i].getCalificaciónActual() >= 5) {
						alumnosAprovados++;
					}
				}
				if (listaEstudiantes[i].getSexo() == "Mujer") {
					if (listaEstudiantes[i].getCalificaciónActual() >= 5) {
						alumnasAprovadas++;
					}
				}
			}
			System.out.println("Alumnos aprovados: " + alumnosAprovados);
			System.out.println("Alumnas aprovadas: " + alumnasAprovadas);
		}
		
		
	}
	

}
