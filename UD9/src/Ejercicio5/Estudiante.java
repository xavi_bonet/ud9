package Ejercicio5;

public class Estudiante extends Persona{
	
	// ATRIBUTOS
	double calificacionActual; //(entre 0 y 10)


	// CONSTRUCTORES
	public Estudiante() {
		super();
		this.calificacionActual = 0;
	}
	
	public Estudiante(double calificaciónActual, String nombre, int edad, String sexo) {
		super(nombre, edad, sexo);
		this.calificacionActual = comprovarCalificacionActual(calificaciónActual);
	}


	// GETTERS Y SETTERS
	public double getCalificaciónActual() {
		return calificacionActual;
	}

	public void setCalificaciónActual(double calificaciónActual) {
		this.calificacionActual = calificaciónActual;
	}

	
	// METODOS
	// falta 50%
	public boolean falta() {
		int num = (int)(Math.random()*101); // Numero random entre el 0 i el 100
		if (num<=50) {
			return true;
		} else {
			return false;
		}
	}
	
	// comprovar si la calificacion introducida esta entre el 0 i el 10
	public double comprovarCalificacionActual(double calificaciónActual) {
		if (calificaciónActual <= 10 && calificaciónActual >= 0) {
			return calificaciónActual;
		} else {
			return 0;
		}
	}
}
