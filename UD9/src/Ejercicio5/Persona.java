package Ejercicio5;

public abstract class Persona {
    
    // ATRIBUTOS
    protected String nombre;
    protected int edad;
    protected String sexo;
    protected boolean faltaAssistencia; //False = no hay falta de asistencia / True = si hay falta de assistencia
    
    
    // CONSTRUCTORES
    public Persona() {
        this.nombre = "";
        this.edad = 0;
        this.sexo = "";
        this.faltaAssistencia = falta();
    }
    
    public Persona(String nombre, int edad, String sexo) {
        this.nombre = nombre;
        this.edad = edad;
        this.sexo = sexo;
        this.faltaAssistencia = falta();
    }

    
    // SETTERS
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

	public void setFaltaAssistencia(boolean faltaAssistencia) {
		this.faltaAssistencia = faltaAssistencia;
	}
	
	
    // GETTERS
    public String getNombre() {
        return nombre;
    }

    public int getEdad() {
        return edad;
    }

    public String getSexo() {
        return sexo;
    }
    
	public boolean getFaltaAssistencia() {
		return faltaAssistencia;
	}
    
	
    // METODOS
	
	// Metodo abstracto falta para definir si un alumno o un professor falta a la clase
    public abstract boolean falta();



    
}