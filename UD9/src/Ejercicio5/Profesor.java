package Ejercicio5;

public class Profesor extends Persona{

	// ATRIBUTOS
	protected String materiaEnseņada;


	// CONSTRUCTORES
	public Profesor() {
		super();
		this.materiaEnseņada = "";
	}
	
	public Profesor(String materiaEnseņada, String nombre, int edad, String sexo) {
		super(nombre, edad, sexo);
		this.materiaEnseņada = comprobarMateriaEnseņada(materiaEnseņada);
	}


	// GETTERS Y SETTERS
	public String getMateriaEnseņada() {
		return materiaEnseņada;
	}

	public void setMateriaEnseņada(String materiaEnseņada) {
		this.materiaEnseņada = materiaEnseņada;
	}
	
	
	// METODOS
	// falta 20%
	public boolean falta() {
		int num = (int)(Math.random()*101); // Numero random entre el 0 i el 100
		if (num<=20) {
			return true;
		} else {
			return false;
		}
	}
	
	// comprovar si la materia introducida es valida (matematicas, filosofia, fisica) si no dejar en blanco
	public String comprobarMateriaEnseņada(String materia) {
		if (materia.toLowerCase() == "matematicas" || materia.toLowerCase() == "filosofia" || materia.toLowerCase() == "fisica" ) {
			return materia;
		} else {
			return "";
		}
	}
	
	
}
