package Ejercicio5;

public class Aula {
	
	// ATRIBUTOS
	private int id;
	private int maxEstudiantes;
	private String materiaDestinada;
	private boolean calaseSiNo;
	private Profesor profesor;
	private Estudiante[] estudiante;
	
	// CONSTRUCTORES
	public Aula() {
		this.id = 0;
		this.maxEstudiantes = 0;
		this.materiaDestinada = "";
	}
	
	public Aula(int id, int maxEstudiantes, String materiaDestinada) {
		this.id = id;
		this.maxEstudiantes = maxEstudiantes;
		this.materiaDestinada = comprobarMateriaDestinada(materiaDestinada);
	}

	public Aula(int id, int maxEstudiantes, String materiaDestinada, Profesor profesor, Estudiante[] estudiante) {
		this.id = id;
		this.maxEstudiantes = maxEstudiantes;
		this.materiaDestinada = comprobarMateriaDestinada(materiaDestinada);
		this.profesor = profesor;
		this.estudiante = estudiante;
	}

	
	// GETTERS
	public int getId() {
		return id;
	}
	public int getMaxEstudiantes() {
		return maxEstudiantes;
	}
	public String getMateriaDestinada() {
		return materiaDestinada;
	}
	public boolean getCalaseSiNo() {
		return calaseSiNo;
	}
	public Profesor getProfesor() {
		return profesor;
	}
	public Estudiante[] getEstudiante() {
		return estudiante;
	}

	
	// SETTERS
	public void setId(int id) {
		this.id = id;
	}
	public void setMaxEstudiantes(int maxEstudiantes) {
		this.maxEstudiantes = maxEstudiantes;
	}
	public void setMateriaDestinada(String materiaDestinada) {
		this.materiaDestinada = materiaDestinada;
	}
	public void setCalaseSiNo(boolean calaseSiNo) {
		this.calaseSiNo = calaseSiNo;
	}
	public void setProfesor(Profesor profesor) {
		this.profesor = profesor;
	}
	public void setEstudiante(Estudiante[] estudiante) {
		this.estudiante = estudiante;
	}

	
	// METODOS
	// comprovar si la materia introducida es valida (matematicas, filosofia, fisica) si no dejar en blanco
	public String comprobarMateriaDestinada(String materia) {
		if (materia.toLowerCase() == "matematicas" || materia.toLowerCase() == "filosofia" || materia.toLowerCase() == "fisica" ) {
			return materia;
		} else {
			return "";
		}
	}
	
	// comprovar si hay classe
	public void comprovarSiHayClase(boolean estadoProfesor, String materiaProfesor, int totalAlumnos, int totalAlumnosClase) {
		if (estadoProfesor == false) {
			if (materiaProfesor == this.materiaDestinada) {
				if (totalAlumnos/2 < totalAlumnosClase) {
					System.out.println("Si, hoy hay Clase");
					this.calaseSiNo = true;
				} else {
					System.out.println("No, no hay suficientes alumnos");
					this.calaseSiNo = false;
				}
			} else {
				System.out.println("No, la materia del profesor no coresponde con la de la clase");
				this.calaseSiNo = false;
			}
		} else {
			System.out.println("No, el profesor no esta disponible");
			this.calaseSiNo = false;
		}
	}
	
	
}
