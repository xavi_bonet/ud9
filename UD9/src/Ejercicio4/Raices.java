package Ejercicio4;

import java.lang.Math.*; 

public class Raices {
	
	// ATRIBUTOS
	private double a;
	private double b;
	private double c;
	
	
	// CONSTRUCTORES
	public Raices(double a, double b, double c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	
	//GETTERS Y SETTERS
	public double getA() {
		return a;
	}

	public void setA(double a) {
		this.a = a;
	}

	public double getB() {
		return b;
	}

	public void setB(double b) {
		this.b = b;
	}

	public double getC() {
		return c;
	}

	public void setC(double c) {
		this.c = c;
	}
	
	
	// METODOS

	// Si discriminante > 0, entonces la ecuaci�n tiene 2 posibles soluciones
	public void obtenerRaices() {
		double discriminante = getDiscriminante();
		if (discriminante > 0) {
		    double x1, x2;
		    x1 = (-b + Math.sqrt(discriminante)) / (2 * a);
		    x2 = (-b - Math.sqrt(discriminante)) / (2 * a);
		    System.out.println("La ra�z real x1 es: " + x1);
		    System.out.println("La ra�z real x2 es: " + x2);
		}
    }
	
	// Si discriminante = 0, entonces la ecuaci�n solo tiene una solucion posible.
    public void obtenerRaiz() {
    	double discriminante = getDiscriminante();
        if (discriminante == 0) {
            double x = -b / (2 * a);
            System.out.println("La ra�z �nica es: " + x);
        }  
    }

    // Devuelve el valor del discriminante
    public double getDiscriminante() {
        double discriminante = (Math.pow(b, 2) - 4 * a * c);
        return discriminante;
    }

	// Devuelve un booleano indicando si tiene dos soluciones.
    public boolean tieneRaices() {
    	double discriminante = getDiscriminante();
    	if (discriminante > 0) {
    		return true;
    	} else {
    		return false;
    	}
    }
    
    // Devuelve un booleano indicando si tiene una �nica soluci�n.
    public boolean tieneRaiz() {
    	double discriminante = getDiscriminante();
        if (discriminante == 0) {
        	return true;
        } else {
        	return false;
        }
    }

    // Mostrara por consola las posibles soluciones que tiene nuestra ecuaci�n, en caso de no existir soluci�n, mostrarlo tambi�n.
    public void calcular() {
    	double discriminante = getDiscriminante();
        if (discriminante >= 0) {
            if (tieneRaiz()) { // Si tiene una �nica soluci�n
                double x = -b / (2 * a);
                System.out.println("La ra�z �nica es: " + x);
            }

            if(tieneRaices()){ // Si tiene dos soluciones.
                double x1, x2;
                x1 = (-b + Math.sqrt(getDiscriminante())) / (2 * a);
                x2 = (-b - Math.sqrt(getDiscriminante())) / (2 * a);
                System.out.println("La ra�z real x1 es: " + x1);
                System.out.println("La ra�z real x2 es: " + x2);
            }
        } else { // Si discriminante < 0, No tiene soluci�n real pero si tiene soluci�n compleja
            System.out.println("No tiene soluci�n real");
        }
    }
    
}
