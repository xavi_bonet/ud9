package Ejercicio4;

public class Ejercicio4TestApp {

	public static void main(String[] args) {

        System.out.println("Numeros: 2, 4, 8");
        Raices raiz1 = new Raices(2,4,8);
        raiz1.calcular();
        
        System.out.println("");
        
        System.out.println("Numeros: 5, 9, 2");
        Raices raiz2 = new Raices(1,6,1);
        raiz2.calcular();
        
        System.out.println("");
        
        System.out.println("Numeros: 1, -2, 1");
        Raices raiz3 = new Raices(1,-2,1);
        raiz3.calcular();
        
	}

}
