package Ejercicio1;

public class Electrodomestico {

	// ATRIBUTOS
	protected double precioBase;
	protected String color;
	protected char consumoEnergetico;
	protected double peso;
	
	private final double defaultPrecioBase = 100;
	private final String defaultColor = "blanco";
	private final char defaultConsumoEnergetico = 'F';
	private final double defaultPeso = 5;
	
	
	// CONSTRUCTORES
	public Electrodomestico() {
		this.precioBase = defaultPrecioBase;
		this.color = comprobarColor(defaultColor);
		this.consumoEnergetico = comprobarLetra(defaultConsumoEnergetico);
		this.peso = defaultPeso; 
	}
	
	public Electrodomestico(double precioBase, double peso) {
		this.precioBase = precioBase;
		this.color = comprobarColor(defaultColor);
		this.consumoEnergetico = comprobarLetra(defaultConsumoEnergetico);
		this.peso = peso; 
	}
	
	public Electrodomestico(double precioBase, String color, char consumoEnergetico, double peso) {
		this.precioBase = precioBase;
		this.color = comprobarColor(color);
		this.consumoEnergetico = comprobarLetra(consumoEnergetico);
		this.peso = peso; 
	}

	
	// GETTERS
	public double getPrecioBase() {
		return precioBase;
	}
	
	public String getColor() {
		return color;
	}

	public char getConsumoEnergetico() {
		return consumoEnergetico;
	}
	
	public double getPeso() {
		return peso;
	}
	
	
	//SETTERS
	public void setPrecioBase(double precioBase) {
		this.precioBase = precioBase;
	}

	public void setColor(String color) {
		this.color = comprobarColor(color);
	}

	public void setConsumoEnergetico(char consumoEnergetico) {
		this.consumoEnergetico = comprobarLetra(consumoEnergetico);
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}
	
	
	// METODOS
	private static String comprobarColor(String color) {
		if (color.toLowerCase() == "blanco" || color.toLowerCase() == "negro" || color.toLowerCase() == "rojo" || color.toLowerCase() == "azul" || color.toLowerCase() == "gris") {
			return color;
		} else {
			return "blanco";
		}
	}
	
	private static char comprobarLetra(char letra) {
		letra = Character.toUpperCase(letra);
		if (letra == 'A' || letra == 'B' || letra == 'C' || letra == 'D' || letra == 'E' || letra == 'F') {
			return letra;
		} else {
			return 'F';
		}
	}
	
	public double precioFinal() {
		double precioBase = getPrecioBase();
		char consumo = getConsumoEnergetico();
		double peso = getPeso();
		
		double precioPorConsumo = 0;
		double precioPorPeso = 0;
		double precioTotal = 0;
		
		if (consumo == 'F') {
			precioPorConsumo = 10;
		} else if (consumo == 'E') {
			precioPorConsumo = 30;
		} else if (consumo == 'D') {
			precioPorConsumo = 50;
		} else if (consumo == 'C') {
			precioPorConsumo = 60;
		} else if (consumo == 'B') {
			precioPorConsumo = 80;
		} else if (consumo == 'A') {
			precioPorConsumo = 100;
		}
		
		if (peso <= 19) {
			precioPorPeso = 10;
		} else if (peso <= 49) {
			precioPorPeso = 50;
		} else if (peso <= 79) {
			precioPorPeso = 80;
		} else if (peso <= 80 || peso > 80) {
			precioPorPeso = 100;
		}
		
		precioTotal = precioBase + precioPorConsumo + precioPorPeso;
		return precioTotal;
	}
	
}
