package Ejercicio1;

public class Lavadora extends Electrodomestico{

	// ATRIBUTOS
	protected double carga;
	 
	private final double defaultCarga = 5;
	
	 
	// CONSTRUCTORES
	public Lavadora () {
		super();
		this.carga = defaultCarga;
	}
	 
	public Lavadora (double precioBase, double peso) {
		super(precioBase, peso);
		this.carga = defaultCarga;
	}
	 
	public Lavadora (double precioBase, String color, char consumoEnergetico, double peso, double carga) {
		super(precioBase, color, consumoEnergetico, peso);
		this.carga = carga;
	}

	 
	// GETTERS
	public double getCarga() {
		return carga;
	}

	
	// SETTERS
	public void setCarga(double carga) {
		this.carga = carga;
	}
	 

	// METODOS
	public double precioFinal() {
		double carga = getCarga();
		double precioTotal = super.precioFinal();
		double precioPorCarga = 0;
		if (carga > 30) {
			precioPorCarga = 50;
		}
		precioTotal = precioTotal + precioPorCarga;
		return precioTotal;
	}
	
	
	 
}
