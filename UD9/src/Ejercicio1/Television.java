package Ejercicio1;

public class Television extends Electrodomestico{
	
	// ATRIBUTOS
	protected int resolucion;
	protected boolean sintonizadorTDT;
	
	private final int defaultResolucion = 20;
	private final boolean defaultSintonizadorTDT = false;
	
	
	// CONSTRUCTORES
	public Television () {
		super();
		this.resolucion = defaultResolucion;
		this.sintonizadorTDT = defaultSintonizadorTDT;
	}
	
	public Television (double precioBase, double peso) {
		super(precioBase, peso);
		this.resolucion = defaultResolucion;
		this.sintonizadorTDT = defaultSintonizadorTDT;
	}
	
	public Television (double precioBase, String color, char consumoEnergetico, double peso, int resolucion, boolean sintonizadorTDT) {
		super(precioBase, color, consumoEnergetico, peso);
		this.resolucion = resolucion;
		this.sintonizadorTDT = sintonizadorTDT;
	}

	
	// GETTERS
	public int getResolucion() {
		return resolucion;
	}
	
	public boolean getSintonizadorTDT() {
		return sintonizadorTDT;
	}
	
	
	// SETTERS
	public void setResolucion(int resolucion) {
		this.resolucion = resolucion;
	}
	
	public void setSintonizadorTDT(boolean sintonizadorTDT) {
		this.sintonizadorTDT = sintonizadorTDT;
	}
	
	
	// METODOS
	public double precioFinal() {
		int resolucion = getResolucion();
		boolean sintonizadorTDT = getSintonizadorTDT();
		double precioTotal = super.precioFinal();
		double incrementoPrecioPorResolucion = 0;
		double precioPorSintonizadorTDT = 0;
		if (resolucion > 40) {
			incrementoPrecioPorResolucion = 30;
		}
		if (sintonizadorTDT == true) {
			precioPorSintonizadorTDT = 50;
		}
		precioTotal = precioTotal + precioPorSintonizadorTDT;
		double cantidadPorcentaje = precioTotal / 100 * incrementoPrecioPorResolucion;
		precioTotal = precioTotal + cantidadPorcentaje;
		return precioTotal;
	}

}
