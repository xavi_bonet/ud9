package Ejercicio1;

public class Ejercicio1TestApp {

	public static void main(String[] args) {
		
		Electrodomestico arrayElectrodomesticos[]=new Electrodomestico[10];
		
		arrayElectrodomesticos[0]=new Lavadora();
		arrayElectrodomesticos[1]=new Lavadora(300, 65);
		arrayElectrodomesticos[2]=new Lavadora(300, "negro", 'C', 45, 35);
		arrayElectrodomesticos[3]=new Lavadora(300, "blanco", 'A', 70, 55);
		arrayElectrodomesticos[4]=new Lavadora(300, 81);
		arrayElectrodomesticos[5]=new Television();
		arrayElectrodomesticos[6]=new Television(500, 15);
		arrayElectrodomesticos[7]=new Television(500, "negro", 'A', 15, 55, true);
		arrayElectrodomesticos[8]=new Television(500, "negro", 'F', 20, 30, false);
		arrayElectrodomesticos[9]=new Television(500, 25);
		
		double precioTotalElectrodomesticos = 0;
		double precioTotalLavadoras = 0;
		double precioTotalTelevisiones = 0;
		
		// Mostrar el precio de cada lavadora en el mismo bloque
		for (int i = 0; arrayElectrodomesticos.length > i; i++) {
			if (arrayElectrodomesticos[i] instanceof Lavadora) {
				double precioLavadora = arrayElectrodomesticos[i].precioFinal();
				System.out.println("Precio Lavadora: " + precioLavadora);
				precioTotalElectrodomesticos = precioTotalElectrodomesticos + precioLavadora;
				precioTotalLavadoras = precioTotalLavadoras + precioLavadora;
			}
		}
		
		System.out.println("");
		
		// Mostrar el precio de cada televicion en el mismo bloque
		for (int i = 0; arrayElectrodomesticos.length > i; i++) {
			if (arrayElectrodomesticos[i] instanceof Television) {
				double precioTelevision = arrayElectrodomesticos[i].precioFinal();
				System.out.println("Precio Television: " + precioTelevision);
				precioTotalElectrodomesticos = precioTotalElectrodomesticos + precioTelevision;
				precioTotalTelevisiones = precioTotalTelevisiones + precioTelevision;
			}
		}
		
		
		System.out.println("");
		
		//Mostrar precios totales
		System.out.println("Precio Electrodomesticos: " + precioTotalElectrodomesticos);
		System.out.println("Precio Lavadoras: " + precioTotalLavadoras);
		System.out.println("Precio Televisiones: " + precioTotalTelevisiones);
	}

}
