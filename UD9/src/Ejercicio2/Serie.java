package Ejercicio2;

public class Serie implements Entregable {

	// ATRIBUTOS
	private String titulo;
	private int numeroDeTemporadas;
	private boolean entregado;
	private String genero;
	private String creador;
	
	final private int DEFAULT_NUMERO_TEMPORADAS = 3;
	final private boolean DEFAULT_ENTREGADO = false;
	
	
	// CONSTRUCTORES
	public Serie() {
		this.titulo = "";
		this.numeroDeTemporadas = DEFAULT_NUMERO_TEMPORADAS;
		this.entregado = DEFAULT_ENTREGADO;
		this.genero = "";
		this.creador = "";
	}
	
	public Serie(String titulo, String creador) {
		this.titulo = titulo;
		this.numeroDeTemporadas = DEFAULT_NUMERO_TEMPORADAS;
		this.entregado = DEFAULT_ENTREGADO;
		this.genero = "";
		this.creador = creador;
	}
	
	public Serie(String titulo, int numeroDeTemporadas, String genero, String creador) {
		this.titulo = titulo;
		this.numeroDeTemporadas = numeroDeTemporadas;
		this.entregado = DEFAULT_ENTREGADO;
		this.genero = genero;
		this.creador = creador;
	}

	
	// GETTERS Y SETTERS
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getNumeroDeTemporadas() {
		return numeroDeTemporadas;
	}

	public void setNumeroDeTemporadas(int numeroDeTemporadas) {
		this.numeroDeTemporadas = numeroDeTemporadas;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getCreador() {
		return creador;
	}

	public void setCreador(String creador) {
		this.creador = creador;
	}

	// METODOS
	@Override
	public String toString() {
		return "Serie [titulo=" + titulo + ", numeroDeTemporadas=" + numeroDeTemporadas + ", entregado=" + entregado + ", genero=" + genero + ", creador=" + creador + "]";
	}
	
	public void entregar() {
		this.entregado = true;
	}
	
	public void devolver() {
		this.entregado = false;
	}
	
	public boolean isEntregado() {
		return entregado;
	}
	
	public boolean compareTo (Object a) {
		Serie s = (Serie)a;
		int value1 = getNumeroDeTemporadas();
		int value2 = s.getNumeroDeTemporadas();
		boolean resultado;
		if (value1 < value2) {
			resultado = true;
		} else {
			resultado = false;
		}
		
		return resultado;
	}
	
	
}
