package Ejercicio2;

public class Ejercicio2TestApp {

	public static void main(String[] args) {
		
		Serie arraySeries[] = new Serie[5];
		Videojuego arrayVideojuego[] = new Videojuego[5];
		
		arraySeries[0] = new Serie("Serie1", 8, "Accion", "A");
		arraySeries[1] = new Serie("Serie2", 7, "Accion", "A");
		arraySeries[2] = new Serie("Serie3", 5, "Accion", "A");
		arraySeries[3] = new Serie("Serie4", 3, "Accion", "A");
		arraySeries[4] = new Serie("Serie5", 10, "Accion", "A");
		
		arrayVideojuego[0] = new Videojuego("Juego1", 45, "Accion", "EA");
		arrayVideojuego[1] = new Videojuego("Juego2", 35, "Accion", "EA");
		arrayVideojuego[2] = new Videojuego("Juego3", 25, "Accion", "EA");
		arrayVideojuego[3] = new Videojuego("Juego4", 55, "Accion", "EA");
		arrayVideojuego[4] = new Videojuego("Juego5", 37, "Accion", "EA");
		
		arraySeries[1].entregar();
		arraySeries[2].entregar();
		arrayVideojuego[2].entregar();
		arrayVideojuego[4].entregar();
		
		int totalSeriesEntregados = 0;
		int totalVideojuegosEntregados = 0;
		
		for (int i = 0; arraySeries.length > i; i++) {
			if (arraySeries[i].isEntregado()) {
				arraySeries[i].devolver();
				totalSeriesEntregados++;
			}
		}
		
		for (int i = 0; arrayVideojuego.length > i; i++) {
			if (arrayVideojuego[i].isEntregado()) {
				arrayVideojuego[i].devolver();
				totalVideojuegosEntregados++;
			}
		}
		
		System.out.println("Series entregadas: " + totalSeriesEntregados);
		System.out.println("Videojuegos entregados: " + totalVideojuegosEntregados);
		
		
		
		//Mostrar el videojuego con mas horas
		int pos1 = 0;
		for (int i = 0; arrayVideojuego.length > i; i++) {
            if(arrayVideojuego[pos1].compareTo(arrayVideojuego[i])) {
            	pos1 = i;
            }
		}
		System.out.println(arrayVideojuego[pos1].toString());
		
		//Mostrar la serie con mas temporadas
		int pos2 = 0;
		for (int i = 0; arraySeries.length > i; i++) {
            if(arraySeries[pos2].compareTo(arraySeries[i])) {
            	pos2 = i;
            }
		}
		System.out.println(arraySeries[pos2].toString());
	}

}
