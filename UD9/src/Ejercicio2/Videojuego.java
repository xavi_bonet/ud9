package Ejercicio2;

public class Videojuego implements Entregable {
	
	// ATRIBUTOS
	private String titulo;
	private int horasEstimadas;
	private boolean entregado;
	private String genero;
	private String compa�ia;
	
	private final int DEFAULT_HORAS_ESTIMADAS = 10;
	private final boolean DEFAULT_ENTREGADO = false;
	
	
	// CONSTRUCTORES
	public Videojuego() {
		this.titulo = "";
		this.horasEstimadas = DEFAULT_HORAS_ESTIMADAS;
		this.entregado = DEFAULT_ENTREGADO;
		this.genero = "";
		this.compa�ia = "";
	}
	
	public Videojuego(String titulo, int horasEstimadas) {
		this.titulo = titulo;
		this.horasEstimadas = horasEstimadas;
		this.entregado = DEFAULT_ENTREGADO;
		this.genero = "";
		this.compa�ia = "";
	}

	public Videojuego(String titulo, int horasEstimadas, String genero, String compa�ia) {
		this.titulo = titulo;
		this.horasEstimadas = horasEstimadas;
		this.entregado = DEFAULT_ENTREGADO;
		this.genero = genero;
		this.compa�ia = compa�ia;
	}

	
	//GETTERS Y SETTERS
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getHorasEstimadas() {
		return horasEstimadas;
	}

	public void setHorasEstimadas(int horasEstimadas) {
		this.horasEstimadas = horasEstimadas;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getCompa�ia() {
		return compa�ia;
	}

	public void setCompa�ia(String compa�ia) {
		this.compa�ia = compa�ia;
	}

	
	@Override
	public String toString() {
		return "Videojuego [titulo=" + titulo + ", horasEstimadas=" + horasEstimadas + ", entregado=" + entregado + ", genero=" + genero + ", compa�ia=" + compa�ia + "]";
	}

	public void entregar() {
		this.entregado = true;
	}
	
	public void devolver() {
		this.entregado = false;
	}
	
	public boolean isEntregado() {
		return entregado;
	}
	
	public boolean compareTo (Object a) {
		Videojuego v = (Videojuego)a;
		int value1 = getHorasEstimadas();
		int value2 = v.getHorasEstimadas();
		boolean resultado;
		if (value1 < value2) {
			resultado = true;
		} else {
			resultado = false;
		}
		
		return resultado;
	}
	
}
