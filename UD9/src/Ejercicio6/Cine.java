package Ejercicio6;

public class Cine {

	// ATRIBUTOS
	protected double precioEntrada;

	final double defaultPrecioEntrada = 10;
	
	
	// CONSTRUCTORES
	public Cine() {
		this.precioEntrada = defaultPrecioEntrada;
	}
	
	public Cine(double precioEntrada) {
		this.precioEntrada = precioEntrada;
	}


	// GETTERS Y SETTERS
	public double getPrecioEntrada() {
		return precioEntrada;
	}

	public void setPrecioEntrada(double precioEntrada) {
		this.precioEntrada = precioEntrada;
	}
	
}
