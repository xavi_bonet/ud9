package Ejercicio6;

public class Ejercicio6TestApp {

	public static void main(String[] args) {

		// Objeto con la informacion de la pelicula
		Pelicula pelicula = new Pelicula ("El Se�or de los Anillos", 13680, 12, "Peter Jackson", 7.5);
		
		// Filas y columnas de los asientos en la sala
		final int FILAS = 10;
		final int COLUMNAS = 9;
		
		// Array bidimensional de asientos
		Asiento asientos[][] = new Asiento[FILAS][COLUMNAS];
		
		// Array de espectadores
		Espectador espectador[] = new Espectador[50];
		
		// Crear espectadores con datos random
		for(int i = 0; i < espectador.length; i++) { 
			espectador[i] = new Espectador("persona" + i, (int)(Math.random()*81), (Math.random()*51));
		}
		
		// Recorrer array bidimensional para asignar las posiciones			// 	Ejemplo
		// x = columna (A, B, C, D, E, F, G, H, I) 							//	1A   	1B   	1C   	1D
		// y = fila (numero)												// 	2A   	2B     	2C   	2D
																			// 	3A   	3B     	3C   	3D	
		for(int x = 0; x < asientos.length; x++) { 
			int i = 1;
			for (int y = 0; y < asientos[x].length; y++) { 
				char letra = '\u0000';
				int numFila = x + 1;
				if (i == 1) {
					letra = 'A';
				} else if (i == 2) {
					letra = 'B';
				} else if (i == 3) {
					letra = 'C';
				} else if (i == 4) {
					letra = 'D';
				} else if (i == 5) {
					letra = 'E';
				} else if (i == 6) {
					letra = 'F';
				} else if (i == 7) {
					letra = 'G';
				} else if (i == 8) {
					letra = 'H';
				} else if (i == 9) {
					letra = 'I';
				}
				asientos[x][y] = new Asiento(letra, numFila);
				i++;
			} 
		}
		
		// Sentar a los espectadores en los asientos aleatoriamente (solo si hay hueco en el cine, si tiene el suficiente dinero, hay espacio libre y tiene edad para ver la pel�cula)
		
		// Variables para comprovar la capacidad de la sala
		int capacidad = FILAS * COLUMNAS;
		int personasDentro = 0;
		
		// Sentar a los espectadores
		for(int i = 0; i < espectador.length; i++) { 
			
			// Comprovar la capacidad de la sala
			if (personasDentro < capacidad) {
				
				// Comprovar si el espectador puede pagar
				if (espectador[i].getDinero() >= pelicula.getPrecioEntrada()) {
					
					// Comprovar si el espectador tiene la edad
					if (espectador[i].getEdad() >= pelicula.getEdadMinima()) {
						
						// Variable para controlar si el espectador esta o no sentado
						boolean sentado = false;
						
						// Sentar en un asiento aleatorio solo si esta libre
						while(!sentado) {
					
							// Variables para sentar al espectador un asiento aleatorio
							int fila = (int)(Math.random()*FILAS);
							int columna = (int)(Math.random()*COLUMNAS);
							
							// Sentar al espectador en un lugar aleatorio si el asiento esta libre, si no buscar un nuevo asiento						
							if (asientos[fila][columna].getDisponivilidad()) {
		                        asientos [fila][columna].setEspectador(espectador[i]);
		                        asientos [fila][columna].setDisponivilidad(false);
		                        sentado = true;
		                        personasDentro++;
							} else {
								sentado = false;
							}
			            }	
					}	
				}
			} else {
				// Si no hay asientos libres para sentar a los siguientes espectadores, mostrar mensaje i romper el bucle
				System.out.println("No hay asientos libres. [ Capacidad = " + personasDentro + " / " + capacidad + " ]");
				System.out.println("");
				break;
			}
		}

		// Mostrar "ASIENTOS OCUPADOS"
		System.out.println("ASIENTOS OCUPADOS = " + personasDentro);
		for(int x = 0; x < asientos.length; x++) { 
			for (int y = 0; y < asientos[x].length; y++) {
				if (!asientos[x][y].getDisponivilidad()) {
					System.out.println(asientos[x][y].toString());
				}
			}
		}
		
		// Mostrar "ASIENTOS LIBRES" solo si los hay
		if (personasDentro < capacidad) {
			System.out.println("");
			System.out.println("ASIENTOS LIBRES = " + (capacidad-personasDentro));
			for(int x = 0; x < asientos.length; x++) { 
				for (int y = 0; y < asientos[x].length; y++) {
					if (asientos[x][y].getDisponivilidad()) {
						System.out.println(asientos[x][y].toString());
					}
				}
			}
		}

		
		
	}

}
