package Ejercicio6;

public class Pelicula extends Cine{

	
	// ATRIBUTOS
	private String titulo;
	private int duracionSegundos;
	private int edadMinima;
	private String director;

	
	// CONSTRUCTORES
	public Pelicula() {
		super();
		this.titulo = "";
		this.duracionSegundos = 0;
		this.edadMinima = 0;
		this.director = "";
	}	
	
	public Pelicula(String titulo, int duracionSegundos, int edadMinima, String director) {
		super();
		this.titulo = titulo;
		this.duracionSegundos = duracionSegundos;
		this.edadMinima = edadMinima;
		this.director = director;
	}	
	
	public Pelicula(String titulo, int duracionSegundos, int edadMinima, String director, double precioEntrada) {
		super(precioEntrada);
		this.titulo = titulo;
		this.duracionSegundos = duracionSegundos;
		this.edadMinima = edadMinima;
		this.director = director;
	}


	// GETTERS Y SETTERS
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public int getDuracionSegundos() {
		return duracionSegundos;
	}
	public void setDuracionSegundos(int duracionSegundos) {
		this.duracionSegundos = duracionSegundos;
	}
	public int getEdadMinima() {
		return edadMinima;
	}
	public void setEdadMinima(int edadMinima) {
		this.edadMinima = edadMinima;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}	
	
	
}
