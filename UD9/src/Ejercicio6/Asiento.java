package Ejercicio6;

public class Asiento extends Cine {
	
	// ATRIBUTOS
	private char columna;
	private int fila;
	private boolean disponibilidad;
	private Espectador espectador;

	
	// CONSTRUCTORES
	public Asiento() {
		super();
		this.columna = '\u0000';
		this.fila = 0;
		this.disponibilidad = true;
		this.espectador = null;
	}
	
	public Asiento(char columna, int fila) {
		super();
		this.columna = columna;
		this.fila = fila;
		this.disponibilidad = true;
	}
	
	public Asiento(char columna, int fila, boolean disponivilidad, Espectador espectador) {
		super();
		this.columna = columna;
		this.fila = fila;
		this.disponibilidad = disponivilidad;
		this.espectador = espectador;
	}
	
	public Asiento(double precioEntrada, char columna, int fila, boolean disponivilidad, Espectador espectador) {
		super(precioEntrada);
		this.columna = columna;
		this.fila = fila;
		this.disponibilidad = disponivilidad;
		this.espectador = espectador;
	}

	
	// GETTERS Y SETTERS
	public char getColumna() {
		return columna;
	}
	public void setColumna(char columna) {
		this.columna = columna;
	}
	public int getFila() {
		return fila;
	}
	public void setFila(int fila) {
		this.fila = fila;
	}
	public boolean getDisponivilidad() {
		return disponibilidad;
	}
	public void setDisponivilidad(boolean disponivilidad) {
		this.disponibilidad = disponivilidad;
	}
	public Espectador getEspectador() {
		return espectador;
	}
	public void setEspectador(Espectador espectador) {
		this.espectador = espectador;
	}

	@Override
	public String toString() {
		return "Asiento [columna=" + columna + ", fila=" + fila + ", disponivilidad=" + disponibilidad + ", espectador=" + espectador + ", precioEntrada=" + precioEntrada + "]";
	}
	
	
	// METODOS
	
	
}
